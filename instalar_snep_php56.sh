#!/usr/bin/env bash 
#   This script was developed by Wélyqrson Bastos Amaral, this license 
# under GPL can be distributed freely. I do not offer any  warranty or 
# support about it use at your own risk.
# Its main functionality is to install an  asterisk-based pabx ip with 
# snep-based graphical web interface.

###############################################################################
WORKDIR=/usr/src/wba
FONT=$(pwd)

DEFAULT_PASS="$(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-12};echo;)"
INSTALL_LOG="${WORKDIR}/Install.log"
SENHAGERADA="${WORKDIR}/Senha-DB.log"
PASSWD_DB_ROOT="${PASSWD_DB_ROOT:-"$DEFAULT_PASS"}"

WWWGROUP="www-data"
WWWUSER="www-data"

# Colors to use for output
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color]'

VERSION_ASTERISK="16-current"
CHECK_HASH_ASTERISK="http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-$VERSION_ASTERISK.sha1"
URL_ASTERISK_SOURCE="http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-$VERSION_ASTERISK.tar.gz"

SNEP_VERSION="3.06.2"
SNEP_URL="https://sourceforge.net/projects/snep/files/snep/snep-3/snep_$SNEP_VERSION.tar.gz"

MYSQL_VERSION="mysql-5.7"

# This source is needed to install mysql-server, since it is now a package maintained by oracle
# source [ Visited on 06/04/2020 ]: https://dev.mysql.com/downloads/file/?id=494003
ADDITIONAL_SOURCE_ORACLE_NAME="mysql-apt-config_0.8.15-1_all.deb"
ADDITIONAL_SOURCE_ORACLE_URL="https://dev.mysql.com/get/$ADDITIONAL_SOURCE_ORACLE_NAME"

BUILD_DEPENDENCY="unixodbc unixodbc-dev mysql-connector-odbc odbcinst1debian2  
libcurl4 libncurses5-dev git php5.6 php5.6-cgi php5.6-mysql  php5.6-gd php5.6-curl build-essential 
lshw libjansson-dev  libssl-dev sox sqlite3 libsqlite3-dev libapache2-mod-php5.6 
libxml2-dev uuid-dev apache2 mysql-server libedit-dev bison wget openssl libssl-dev 
libasound2-dev libc6-dev  libxml2-dev libsqlite3-dev libnewt-dev libncurses5-dev 
zlib1g-dev gcc  g++ make perl uuid-dev git subversion libjansson-dev unixodbc-dev  
unixodbc-bin unixodbc rsync php5.6-xml"


ORACLE_PRESENT_SOURCE_PACKAGE=$(dpkg -l|grep mysql-apt-config >/dev/null; echo $?)

# Dados da distribuição
source /etc/os-release 

###############################################################################

updateSystem(){

	apt update 
	apt dist-upgrade -y 
	apt install -y zip gnupg unzip net-tools htop vim ctags tmux git ack subversion apt-transport-https lsb-release ca-certificates curl 
	wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
	echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list
	apt dist-upgrade -y && apt autoremove -y 	


}

installAptSourceOracle(){

	cd /usr/src/

	if [ "${ORACLE_PRESENT_SOURCE_PACKAGE}" -eq "1" ] 
	then
		echo "Tentando instalar pacote de fonte apt Oracle!..."

		wget -c $ADDITIONAL_SOURCE_ORACLE_URL
		if [ "$?" -eq  "0" ] 
		then
			# Configurando automação  da instalação do pacote
			export DEBIAN_FRONTEND="noninteractive"; 
			echo mysql-apt-config mysql-apt-config/select-server select ${MYSQL_VERSION}    | debconf-set-selections;
			echo mysql-apt-config mysql-apt-config/tools-component select mysql-tools| debconf-set-selections;
			echo mysql-apt-config mysql-apt-config/select-preview select Disabled    | debconf-set-selections; 
			echo mysql-apt-config mysql-apt-config/select-product select Ok          | debconf-set-selections;

			dpkg -i $ADDITIONAL_SOURCE_ORACLE_NAME 
			apt update
		else
			echo "Não foi possível fazer o download..."
			echo "Consulte no site da ORACLE https://dev.mysql.com "
			exit 1
		fi
	else
		echo "Pacote adicional ORACLE já instalado..."
	fi
}

initialSetting(){


	if [ ! -d "$WORKDIR" ]
   	then
		mkdir -p "$WORKDIR"
	fi
	touch $SENHAGERADA

	echo "
	Senha gerada: $DEFAULT_PASS 
	
	" >> $SENHAGERADA

}

acquireFontsAndUnzip(){
	local COUNTER=0

	# Start process pre-compile fonts asterisk 
	cd $WORKDIR

	wget -c $SNEP_URL -O snep.tar.gz 
	tar -xzf snep.tar.gz 
	find . -type d -name 'snep*' -exec  mv {} snep \;

	# Get Asterisk Fonts
	while true; do
		if [[ ! -f "asterisk-${VERSION_ASTERISK}.tar.gz" && 
			! -f "asterisk-${VERSION_ASTERISK}.sha1" ]] 
		then
			wget -c $URL_ASTERISK_SOURCE 
			wget -c $CHECK_HASH_ASTERISK 
		fi
	HASH_FILE=$(sha1sum "asterisk-${VERSION_ASTERISK}.tar.gz"| \
		awk '{print $1}')

	HASH_WEB=$(cat "asterisk-${VERSION_ASTERISK}.sha1"| \
		awk '{print $1}')

	if [[ ${HASH_WEB} == ${HASH_FILE}  ]] 
	then
		tar xzf asterisk-*.tar.gz
		break
	else
		clear
		echo -e "
		Houston, we may have a problem!
		Trying course correction and...
		downloading again!

		"
		sleep 2

		if [[ $COUNTER -lt 3 ]] 
		then
			rm -f asterisk-${VERSION_ASTERISK}.sha1  >/dev/null
			rm -f asterisk-${VERSION_ASTERISK}.tar.gz >/dev/null 

			((COUNTER++))
		else
			echo -e "
			Houston, we have a problem!			
			Please find the reason manually ... 
			unable to continue!

			"		
			exit 1
		fi

		fi
	done
}

configPhpIni(){

	# recomendação do time de desenvolvimento snep para ativar register_argc_argv 
	find /etc/  -type f -name php.ini -exec sed -i \
		"/register_argc_argv/s/register_argc_argv.*/register_argc_argv = on/g" \
		{} \; 

	# aumentar tamanho do upload máximo 
	find /etc/  -type f -name php.ini -exec sed -i \
		"/upload_max_filesize/s/upload_max_filesize.*/upload_max_filesize = 40M /g" \
		{} \; 
	find /etc/  -type f -name php.ini -exec sed -i \
		"/post_max_size/s/post_max_size.*/post_max_size = 40M /g" \
		{} \; 

	# ajustando timezone php
	find /etc/  -type f -name php.ini -exec sed -i \
		 "s@;date.timezone =@date.timezone = America/Porto_Velho@" {} \; 
}

installDependencies(){

	export DEBIAN_FRONTEND="noninteractive";
	debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password ${PASSWD_DB_ROOT}"
	debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password ${PASSWD_DB_ROOT}"

	apt install -y ${BUILD_DEPENDENCY} 

}

compileAsterisk(){

	cd $WORKDIR 
	ASTERISK_DIR=$(find . -maxdepth 1 -type d -name '*sterisk*'  -print |cut -c3-)

	cd $ASTERISK_DIR

	make clean
	
	./configure 

	make menuselect.makeopts
	menuselect/menuselect --enable app_voicemail --enable ODBC_STORAGE menuselect.makeopts
	menuselect/menuselect --enable format_mp3 --enable codec_ilbc --enable format_ilbc  menuselect.makeopts

	contrib/scripts/get_mp3_source.sh	
	
	make && make install && make config && make samples && make progdocs
}

configSnep(){

	cd $WORKDIR 

	SNEP_DIR=$(find . -maxdepth 1 -type d -name 'snep*'  -print |cut -c3-)
	HTML_HOME="/var/www/html/"
	SNEP_HOME="${HTML_HOME}${SNEP_DIR}"
	
	cp -rva "$SNEP_DIR" "$HTML_HOME"

	find ${SNEP_HOME} -type f -exec chmod 640 {} \; -exec chown ${WWWUSER}:${WWWGROUP} {} \;
	find ${SNEP_HOME} -type d -exec chmod 775 {} \; -exec chown ${WWWUSER}:${WWWGROUP} {} \;
	
	mkdir /var/log/snep
	touch /var/log/snep/ui.log
	touch /var/log/snep/agi.log
	chown -Rc ${WWWUSER}:${WWWGROUP} /var/log/snep 

	cd "${SNEP_HOME}" 
	ln -s /var/log/snep  logs 
	
	cd /var/lib/asterisk/agi-bin/ 
	ln -s "${SNEP_HOME}/agi/" snep 
	cd snep 
	chmod +x *.php 
	

	cp "${SNEP_HOME}/install/snep.apache2" /etc/apache2/sites-available/001-snep.conf 
	a2dissite 000-default.conf	
	a2ensite 001-snep.conf  
	systemctl reload apache2
	
	cd /var/spool/asterisk/ 
	rm -rvf monitor 
	ln -sf "${SNEP_HOME}/arquivos" monitor  
	
	cd /etc/
 
	[[ ! -d "/etc/asterisk.old" ]] && mv asterisk asterisk.orig

	mkdir /etc/asterisk 

	rsync -a -r --progress --delete-after  "${SNEP_HOME}/install/etc/asterisk/." /etc/asterisk/.
	
	cp -bvaf ${SNEP_HOME}/install/etc/odb* /etc/
	patch -p0 < ${FONT}/odbcinst.ini.patch
	patch -p0 < ${FONT}/odbc.ini.patch

	cd "${SNEP_HOME}/install/sounds" 
	
	[[ ! -d "/var/lib/asterisk/sounds/en/backup"    ]] && mkdir -p /var/lib/asterisk/sounds/{en,en/tmp,en/backup}
	[[ ! -d "/var/lib/asterisk/sounds/es/backup"    ]] && mkdir -p /var/lib/asterisk/sounds/{es,es/tmp,es/backup}
	[[ ! -d "/var/lib/asterisk/sounds/pt_BR/backup" ]] && mkdir -p /var/lib/asterisk/sounds/{pt_BR,pt_BR/tmp,pt_BR/backup} 
	[[ ! -d "/var/lib/asterisk/moh/backup" ]] && mkdir -p /var/lib/asterisk/moh/{tmp,backup} 
	
	tar -xzf asterisk-core-sounds-en-wav-current.tar.gz  -C /var/lib/asterisk/sounds/en
	tar -xzf asterisk-extra-sounds-en-wav-current.tar.gz -C /var/lib/asterisk/sounds/en 
	tar -xzf asterisk-core-sounds-es-wav-current.tar.gz  -C /var/lib/asterisk/sounds/es
	tar -xzf asterisk-core-sounds-pt_BR-wav.tgz          -C /var/lib/asterisk/sounds/pt_BR

	chown -Rc ${WWWUSER}:${WWWGROUP} /var/lib/asterisk/sounds 

	cd /var/lib/asterisk/moh
	chown -Rc ${WWWUSER}:${WWWGROUP} /var/lib/asterisk/moh 
	rm -vf *-asterisk-moh-opsound-wav

	mkdir -p "${SNEP_HOME}/sounds" 
	cd "${SNEP_HOME}/sounds" 
	ln -sf /var/lib/asterisk/moh/ moh 
	ln -sf /var/lib/asterisk/sounds/pt_BR/ pt_BR 

	cd "${SNEP_HOME}/install/database"
	patch -p0 < ${FONT}/system_data.sql.patch
	patch -p0 < ${FONT}/schema.sql.patch
	
	mysql -u root -p${PASSWD_DB_ROOT} < database.sql
	mysql -u root -p${PASSWD_DB_ROOT} snep < schema.sql
	mysql -u root -p${PASSWD_DB_ROOT} snep < system_data.sql
	mysql -u root -p${PASSWD_DB_ROOT} snep < core-cnl.sql
	mysql -u root -p${PASSWD_DB_ROOT} snep < ../../modules/billing/install/schema.sql 
	mysql -u root -p${PASSWD_DB_ROOT} snep < ../../modules/loguser/install/schema.sql 

}

###############################################################################

# Definir Log
exec 1>> >(tee -a $INSTALL_LOG )
exec 2>&1

initialSetting
cd $WORKDIR

echo "Atualizando sistema..."
updateSystem 

echo "Instalação de dependencias..."
installAptSourceOracle 

echo "Instalando dependencias ..."
installDependencies 

echo "Configurando php ..."
configPhpIni 

echo "Baixando e descompactando fontes ..."
acquireFontsAndUnzip 

echo "Compilando asterisk..."
compileAsterisk 

echo "Instalando app snep..."
configSnep

systemctl enable asterisk.service
systemctl enable mysql.service
systemctl enable apache2.service

systemctl restart asterisk.service
systemctl restart mysql.service
systemctl restart apache2.service

clear
echo "
Instalação concluida.
Senha do usuário root mysql é $PASSWD_DB_ROOT
"
